import { Component } from '@angular/core';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'my-app',
  styleUrls: ['./app.component.css'],
  templateUrl: './app.component.html',
})
export class AppComponent  {
  name = 'Angular';
  image0 : String = "";
  image1 : String = "../assets/images/ipad-tablet-and-macbook-laptop-workspace-from-above-picjumbo-com.jpg";
  image2 : String = "../assets/images/closed-macbook-laptop-on-a-sofa-picjumbo-com.jpg";
  image3 : String = "../assets/images/white-iphone-5s-with-headphones-picjumbo-com.jpg";
  image4 : String = "../assets/images/new-iphone-5s-gold-in-cafe-picjumbo-com.jpg";
  index : Integer = 0;
  listImage = [ this.image1, this.image2, this.image3, this.image4 ];
  imageCarousel : String = this.listImage[0];
  twoWaysBindingModel : String = "";
  clickAfficheText : String = "";
  listTodo = [];
  // listTodo: any = new Map<any, String>();
  today = Date.now();

  testMethod() {
    alert("Test réussi");
  }

  changeImage() {
    this.image0 = this.image1;
    this.image1 = this.image2;
    this.image2 = this.image0;
  }

  zoomMinus() {
    var width = document.getElementById('image1').offsetWidth;
    width = width - 100;
    document.getElementById('image1').style.width = width + "px";
  }

  zoomPlus() {
    var width = document.getElementById('image1').offsetWidth;
    width = width + 100;
    document.getElementById('image1').style.width = width + "px";
  }

  previous() {
    this.index--;
    if ( this.index < 0 ) {
      this.index = this.listImage.length - 1;
    }
    this.imageCarousel = this.listImage[this.index];
  }

  next() {
    this.index++;
    if ( this.index >= this.listImage.length ) {
      this.index = 0;
    }
    this.imageCarousel = this.listImage[this.index];
  }

  clickAlert() {
    alert(this.twoWaysBindingModel);
  }

  clickAffiche() {
    this.clickAfficheText = this.twoWaysBindingModel;
  }

  clickTodo() {
    this.listTodo.push({this.today, this.twoWaysBindingModel});
    // this.listTodo.set(this.today, this.twoWaysBindingModel);
    // this.listTodo.push(this.twoWaysBindingModel);
  }

  getKeys(this.listTodo) {
    return Array.from(this.listTodo.keys());
  }

  getValues(this.listTodo) {
    return Array.from(this.listTodo.values());
  }

  removeLastTodo() {
    // this.listTodo.splice(-1,1);
  }

  validTodo() {

  }

}
